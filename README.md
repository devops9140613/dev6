#### 1) В качестве СУБД используется Postgresql
#### 2) Чтобы запустить миграцию нужно выполнить файл migrationsUp.go
#### 3) Чтобы вернуть БД в исходное состояние нужно выполнить файл migrationsDown.go
#### 4) Для запуска проекта нужно запустить файл main.go
#### 5) Чтобы записать данные в БД необходимо послать POST запрос к серверу (в моем случае localhost:8080/v1/products/add) с необходимым телом: 
 ```json
 {
 "id":"ProductID",
 "name":"ProductName",
 "price":"ProductPrice",
 "quantity":"ProductQuantity"
} 
```
