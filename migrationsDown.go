package main

import (
        "log"

        "github.com/golang-migrate/migrate"
        _ "github.com/golang-migrate/migrate/source/file"
        _ "github.com/golang-migrate/migrate/database/postgres"
)

func main() {
        m, err := migrate.New(
                "file://db/migrations",
                "postgres://"db_user":"db_pass"@localhost:5432/"db_name"?sslmode=disable")
        if err != nil {
                log.Fatal(err)
        }
        if err := m.Down(); err != nil {
                log.Fatal(err)
        }
}

